package hwo2014

import scala.annotation.tailrec

/**
 * Created by liosedhel-arch on 19.04.14.
 */
class SuperBot(val raceData: RaceData, val myCarId: CarId) extends Calculations {

    println(raceData);
    var strategyMap: Map[PiecePosition, MsgWrapper] = Map()

    var lastMessage: MsgWrapper = _
    var currentThrottle: Double = 1

    def calculateMessage(carPositions: List[CarPosition]): MsgWrapper = {
        val myCarPosition = carPositions.filter(_.id == myCarId).head
        val logChange: MsgWrapper => MsgWrapper = changeMessage(myCarPosition.piecePosition);
       // changeThrottle(nextThrottle(100.0, myCarPosition, currentThrottle), logChange)
        raceData.track.pieces(myCarPosition.piecePosition.pieceIndex) match {
          case Turn(_, _) => changeThrottle(nextThrottleFromTurn(10.0, myCarPosition, currentThrottle), logChange)
          case Straight(_, _) => changeThrottle(nextThrottleFromStraight(150.0, myCarPosition, currentThrottle), logChange)
        }
    }


    private def changeMessage(piecePosition: PiecePosition)(message: MsgWrapper): MsgWrapper = {
           if(message != lastMessage) {
             strategyMap = strategyMap + (piecePosition -> message)
           }
           message
    }

    def changeThrottle(throttle: Double, logChange: MsgWrapper => MsgWrapper): MsgWrapper = {
      currentThrottle = throttle;
      logChange(MsgWrapper("throttle", throttle))
    }


}


trait Calculations {
  val raceData: RaceData

  lazy val pieces = raceData.track.pieces;

  def isBeforeTurn(carPosition: CarPosition) = {
     raceData.track;
  }
  def nextThrottleFromTurn(ahead: Double, carPosition: CarPosition, currentThrottle: Double): Double = {
    whatIsNext(ahead, carPosition, (currentPiece: Piece) => currentPiece.isInstanceOf[Straight]) match {
      case Turn(_, _) => 0.2
      case Straight(_, _) => 0.6
    }
  }

  def nextThrottleFromStraight(ahead: Double, carPosition: CarPosition, currentThrottle: Double): Double = {
    whatIsNext(ahead, carPosition, (currentPiece: Piece) => currentPiece.isInstanceOf[Turn]) match {
      case Turn(_, _) =>  0.2
      case Straight(_, _) => 1
    }
  }

  def whatIsNext(ahead: Double, carPosition: CarPosition, additional: Piece => Boolean): Piece = {
    find(ahead, carPosition.piecePosition.pieceIndex, carPosition.piecePosition.inPieceDistance, additional)
  }

  @tailrec private def find(ahead: Double, pieceIndex: Int, inPiecePosition: Double, additional: Piece => Boolean): Piece = {

    val currentPieceIndex = if(pieces.length == pieceIndex) 0 else pieceIndex

    val currentPiece = pieces(currentPieceIndex)
    val pieceLength = currentPiece.length - inPiecePosition
    val newAhead = ahead - pieceLength

    if(newAhead <= 0 || additional(currentPiece)) currentPiece else find(ahead, currentPieceIndex + 1, 0, additional)
  }

}
