package hwo2014

import org.json4s._
import org.json4s.DefaultReaders._
import hwo2014.JSonAdditionalOperations.JValueExtended

/**
 * Created by liosedhel on 4/16/14.
 */
object JSonAdditionalOperations {
    /*
  because implicit classes are soo cool ;D
   */
  implicit class JValueExtended(value: org.json4s.JsonAST.JValue) {
    def has(childString: String): Boolean = {
      if ((value \ childString) != JNothing) {
        true
      } else {
        false
      }
    }
  }
}

case class RaceData(val track: Track, val cars: List[Car], val raceSession: RaceSession)

object RaceData {
  def create(data: JValue): RaceData = {

       val race: JValue = data \ "race"
       val track: JValue = race \ "track"
       val cars: JValue = race \ "cars"
       val raceSession: JValue = race \ "raceSession"
       import hwo2014.JSonAdditionalOperations._
       return new RaceData(
          new Track(track),
          cars.children.map(car => new Car(car)),
          new RaceSession(
              (raceSession \ "laps").as[Int],
              (raceSession \ "maxLapTimeMs").as[Int],
              (raceSession \ "quickRace").getAs[Boolean].getOrElse(false))
          )
  }
}

case class Track(val id: String, val name: String, val pieces: List[Piece], val lanes: List[Lane]) {
    def this(track: JValue) = {
      this(
        (track \ "id").as[String],
        (track \ "name").as[String],
        (track \ "pieces").children.map(piece => {
          if(piece.has("length")) {
              Straight((piece \ "length").as[Double], (piece \ "switch").getAs[Boolean].getOrElse(false))
          } else {
              Turn((piece \ "radius").as[Double], (piece \ "angle").as[Double])
          }
        }),
        (track \ "lanes").children.map(lane => {
          Lane((lane \ "distanceFromCenter").as[Double], (lane \ "index").as[Int])
        })
      )
    }

}

abstract class Piece {
  def length: Double
}
case class Turn(val radius: Double, val angle: Double) extends Piece {
  def length: Double = {
    ((2 * Math.PI * radius) * angle / 360)
  }
}
case class Straight(val length: Double, val switch: Boolean) extends Piece
case class Lane(val distanceFromCenter: Double, val index: Int)

case class Car(id: CarId, length: Double, width: Double, val guideFlagPosition: Double) {
  def this(car: JValue) {
    this(
      CarId((car \ "id" \ "name").as[String], (car \ "id" \ "color").as[String]),
      (car \ "dimensions" \ "length").as[Double],
      (car \ "dimensions" \ "width").as[Double],
      (car \ "dimensions" \ "guideFlagPosition").as[Double]
      )
  }
}

case class RaceSession(laps: Int, maxLapTimeMs: Int, quickRace: Boolean)

case class CarId(name: String, color: String)
