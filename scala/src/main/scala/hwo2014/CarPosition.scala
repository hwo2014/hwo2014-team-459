package hwo2014

import org.json4s.JsonAST.JValue
import org.json4s.DefaultReaders._

/**
 * Created by liosedhel-arch on 19.04.14.
 */
case class CarPosition(val id: CarId, val angle: Double, val piecePosition: PiecePosition) {
  def this(data: JValue) = {
    this(
      CarId((data \ "id" \ "name").as[String] ,(data \ "id" \ "color").as[String]),
      (data \ "angle").as[Double],
      new PiecePosition(data \ "piecePosition")
    )
  }
}

case class PiecePosition(val pieceIndex: Int, val inPieceDistance: Double, val lane: LaneInPiece,  val lap: Int) {
  def this(data: JValue) = {
    this(
      (data \ "pieceIndex").as[Int],
      (data \ "inPieceDistance").as[Double],
      new LaneInPiece(data \ "lane"),
      (data \ "lap").as[Int]
    )
  }
}

case class LaneInPiece(val startLaneIndex: Int, val endLaneIndex: Int) {
  def this(data: JValue) = {
    this(
      (data \ "startLaneIndex").as[Int],
      (data \ "endLaneIndex").as[Int]
    )
  }
}

