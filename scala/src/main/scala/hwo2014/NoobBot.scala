package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec
import org.json4s.DefaultReaders._

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  send(MsgWrapper("join", Join(botName, botKey)))
  play

  var superBoot: SuperBot = _
  var yourCarId: CarId = _

  @tailrec private def play {
    val line = reader.readLine()
    var message: MsgWrapper = MsgWrapper("throttle",0.5)

    if (line != null) {
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("carPositions", data) => message = if(superBoot != null) superBoot.calculateMessage(data.children.map(new CarPosition(_))) else MsgWrapper("throttle",0.5)
        case MsgWrapper("gameInit", data) => superBoot = new SuperBot(RaceData.create(data), yourCarId);
        case MsgWrapper("yourCar", data) => yourCarId = CarId((data \ "name").as[String], (data \ "color").as[String])
        case MsgWrapper("dnf", data) => {
          println("Dyskwalifikacja")
          println(data)
        }
        case MsgWrapper(msgType, _) =>
          println("Received: " + msgType)
      }
      send(message)
      play
    }
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }

}

case class Join(name: String, key: String)
case class MsgWrapper(msgType: String, data: JValue) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats{}


  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
